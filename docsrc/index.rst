.. Zerpent documentation master file, created by
   sphinx-quickstart on Tue Aug 21 08:17:28 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Zerpent's documentation!
===================================

Zerpent is a bootstrap project voor Zaaksysteem Python (micro)services.

.. toctree::
    :maxdepth: 2
    :caption: Contents:

    modules.rst
    action.rst

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
