### Stage 1: Setup base python with requirements
FROM python:3.7-slim-stretch AS base-layer

# Temp location: make sure we cache this bitch
RUN pip install ipython

# Copy requirements before copying complete source, to allow caching
COPY requirements/base.txt /tmp
RUN cd /tmp && pip install -r base.txt

# Setup application
COPY . /opt/src
WORKDIR /opt/src

COPY docsrc/* docs/
RUN sphinx-apidoc -f -o docs zerpent && sphinx-build -j 4 -a docs docs/_build/html

FROM python:3.7-slim-stretch AS production

WORKDIR /opt/zerpent

COPY --from=base-layer /usr/local /usr/local
COPY --from=base-layer /opt/src/*.md ./
COPY --from=base-layer /opt/src/*.txt ./
COPY --from=base-layer /opt/src/zerpent ./zerpent
COPY --from=base-layer /opt/src/docs/_build/html ./docs

ENV FLASK_APP=zerpent

ENV OTAP=production FLASK_ENV=production

CMD [ "flask", "run", "--host=0.0.0.0" ]

FROM production AS development-requirements

WORKDIR /tmp
COPY requirements/test.txt /tmp

RUN pip install -r test.txt

FROM production AS development

COPY --from=development-requirements /usr/local /usr/local

ENV OTAP=development FLASK_ENV=development
