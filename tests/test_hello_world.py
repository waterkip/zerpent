import pytest


def throw_something():
    raise SystemExit(1)


def test_mytest():
    with pytest.raises(SystemExit):
        throw_something()
