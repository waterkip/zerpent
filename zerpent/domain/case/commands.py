from uuid import UUID

from zerpent.domain.command import Command


class CaseCommand(Command):
    domain = "case"


class TransitionCaseCommand(Command):
    """Command data structure for the case transition operation."""

    case_id: UUID
