"""A Zaaksysteem Python bootstrap application

.. moduleauthor:: Michiel Ootjers <michiel@mintlab.nl>

"""

__version__ = "0.0.1"

import flask_statsdclient
import logging
import sys
import json_logging

from decouple import config
from flask import Flask
from zerpent.app.http.case import case
from zerpent.app.http.error_handlers import endpoint_not_found
from zerpent.app.http.error_handlers import internal_server_error
from zerpent.app.http.root import root
from zerpent.docs import docs

app = Flask(__name__)

OTAP_STATE = config("OTAP", default="development")

if OTAP_STATE != "development":
    json_logging.ENABLE_JSON_LOGGING = True
    json_logging.init(framework_name="flask")
    json_logging.init_request_instrument(app)

    statsd = flask_statsdclient.StatsDClient()
    statsd.init_app(app)

logger = logging.getLogger("flask.app")
logger.setLevel(logging.DEBUG)
logger.addHandler(logging.StreamHandler(sys.stdout))

app.register_blueprint(root, url_prefix="/")
app.register_blueprint(docs, url_prefix="/docs")
app.register_blueprint(case, url_prefix="/case")

app.register_error_handler(404, endpoint_not_found)
app.register_error_handler(500, internal_server_error)
