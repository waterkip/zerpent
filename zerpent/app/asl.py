from uuid import UUID

from zerpent.domain.command import Command
from zerpent.domain.service import Service
from zerpent.infrastructure.command_source import Journal


def dispatch(command: Command, service: Service) -> UUID:
    """Event sourced command dispatcher
    """
    journal = Journal()
    entry_id = journal.add_entry(command)

    service.execute(command, context_id=entry_id)

    return entry_id
